﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
       
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float MoveX = Input.GetAxis("Mouse X");
       

        Vector3 movement = new Vector3(transform.position.x + MoveX, 0.0f, 0.0f);

        transform.position = movement;
        rb.position = new Vector3
       (
           Mathf.Clamp(rb.position.x, -23.9f, 23.9f),
           0.0f,
          0.0f
       );
    }
}
