﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BallController : MonoBehaviour
{

    private Rigidbody rb;
    private bool beginGame;
    public GameController gameController;
    private int life;
    public Text LifeText;

    // Use this for initialization
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    
        rb = GetComponent<Rigidbody>();
        beginGame = false;
        life = 3;
        AfficherVie(life);
        }

    void FixedUpdate()
    {
      if (Input.GetButton("Fire1") && beginGame == false)
        {
            rb.velocity = new Vector3(10.0f, 0.0f, 10.0f);
            gameObject.transform.parent = null;
            beginGame = true;
        }  
    }


    void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.tag == "Player")
        {
            float x = Hitpart(transform.position, collision.collider.transform.position);
            Vector3 vec = new Vector3(x * 20, 0, 20).normalized;
            rb.velocity = vec * 15;
        }

        if (collision.collider.tag == "Brick")
        {
            
            Destroy(collision.collider.gameObject);
        }

        if (collision.collider.tag == "Fall")
        {
            if (life > 0)
            {
                life--;
                AfficherVie(life);
                gameController.InitGame();
            }else
            {
                gameController.EndGame();
            }
          
        }

    }
    float Hitpart(Vector3 ballPosition, Vector3 paddlePosition) {
		return (ballPosition.x - paddlePosition.x);
	}

    public void setBeginGame(bool begin)
    {
        beginGame = begin;
    }

    public void AfficherVie(int life)
    {
        LifeText.text = "Nombre de Vie: " + life;
    }

}
