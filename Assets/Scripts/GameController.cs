﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    private int[,] lvl1 = { { 1, 1, 1, 1, 1, 1 },
                            { 1, 0, 0, 0, 0, 1},
                            { 1, 1, 1, 1, 1, 1 }
                          };

    private int[,] lvl2 = { { 1, 0, 1, 0, 1, 1 },
                            { 1, 1, 0, 1, 0, 1 },
                            { 1, 0, 1, 0, 1, 1 }
                          };
    
    public Text text;
    public GameObject brick;
    public GameObject ball;
    public GameObject paddle;
    public BallController ballController;

    void Start()
    {
        ballController = ball.GetComponent <BallController>();
        loadlvl(lvl1);
    }

    void loadlvl(int [,] lvl)
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (lvl[i, j] == 1)
                {
                    Instantiate(brick, new Vector3(j * 8.0F - 20, 0, i * 5.0F + 15), Quaternion.identity);
                }
            }
        }
    }

    void Update()
    {
        if (GameObject.FindWithTag("Brick") == null)

        {
            InitGame();
            loadlvl(lvl2);

        }

        if (Input.GetKey(KeyCode.Escape) && SceneManager.GetActiveScene().buildIndex == 1)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void InitGame()
    {
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.transform.SetPositionAndRotation(new Vector3(paddle.transform.position.x, 0.0f, 1.6f), Quaternion.identity);
        ball.transform.SetParent(paddle.GetComponent<Rigidbody>().transform);
        ballController.setBeginGame(false);

    }

    public void EndGame()
    {
        SceneManager.LoadScene(0);
    }
}
